#ifndef MOTION_H
#define MOTION_H

#include <stdint.h>

#define MAX_MOTIONS 8
#define MAX_SEGMENTS (3*MAX_MOTIONS)

#define DIRECTION_CW 0
#define DIRECTION_CCW 1

// A motion is a movement with a distance measured in steps. When we take these
// steps, we are starting at some initial velocity v_init, and we don't want to
// exceed some maximum velocity v_max. Velocity is measured in steps/s. We also
// can't instantly reach the max velocity, we need to accelerate and decelerate.
// Acceleration and decelaration are currently done at the same rate, measured
// in steps/s^2 (how much our velocity in steps/s changes each second).
struct motion {
  int steps;
  int direction;
  int v_init;
  int v_max;
  int accel;

  // Used for moves with unbounded steps. If wait is non-NULL, we will
  // accelerate and then coast until wait is set to 0.
  volatile uint8_t* wait;
};

#endif // MOTION_H

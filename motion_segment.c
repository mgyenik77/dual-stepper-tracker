#include "motion_segment.h"
#include "sqrt.h"

uint32_t compute_t0_microseconds(struct motion* m) {
  float a = (float)m->accel;
  float t0 = f32_sqrt(2.0f/a);
  return (int)(t0*1000000.0f);
}

void segment_queue_init(struct segment_queue* sq) {
  sq->read = 0;
  sq->write = 0;
}

void segment_queue_add(struct segment_queue* sq, int type, int steps, struct motion* m) {
  // Get the next segment index, but don't actually write the incremented index
  // until the segment is fully prepared.
  int next_segment_idx = (sq->write + 1);
  if (next_segment_idx >= MAX_SEGMENTS) {
    next_segment_idx -= MAX_SEGMENTS;
  }

  // Initialize the motion segment with the steps to take and whether we should
  // accelerate, decelerate, or coast. We initialize steps taken to 1 because
  // when the interrupt handler fires for an update event, it will be after the
  // step is taken, so the first handler that fires after the first step sees
  // that 1 step has been taken so far.
  struct motion_segment* seg = &sq->segments[sq->write];
  seg->type = type;
  seg->steps_taken = 0;
  seg->total_steps = steps;
  if (type == COAST) {
    seg->t0_us = 1000000.0f/m->v_max;
  } else {
    seg->t0_us = compute_t0_microseconds(m);
  }

  // Keep a copy of the motion parameters for easy access in the interrupt
  // handler. The motion struct we have a pointer to might be gone when we are
  // actually executing the move.
  seg->m = *m;

  // Increment write index only when the segment is ready.
  sq->write = next_segment_idx;
}

void segment_queue_discard_last(struct segment_queue* sq) {
  sq->read++;
  if (sq->read >= MAX_SEGMENTS) {
    sq->read = 0;
  }
}

int segment_queue_size(struct segment_queue* sq) {
  int size = sq->write - sq->read;
  if (size < 0) {
    size += MAX_SEGMENTS;
  }
  return size;
}

struct motion_segment* segment_queue_current(struct segment_queue* sq) {
  return &sq->segments[sq->read];
}

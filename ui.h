#ifndef UI_H
#define UI_H

#include <stdint.h>

// Initialize timer and GPIO needed to read UI hardware.
void ui_init();

// Safely reads current event bitmask with interrupts disabled.
uint16_t ui_get_events();

// Pressed, released, and encoder events are kept in a 16 bit bitmap. The
// following functions are helpers to pack and unpack the events from the
// bitmap.
//
// The bits used are:
//   [0..3] = buttons0-3 pressed
//   [4..7] = buttons0-3 released
//   8 = encoder pressed
//   9 = encoder released
//   10 = encoder cw
//   11 = encoder ccw
uint16_t set_button_pressed(uint16_t bits, int button);
uint16_t set_button_released(uint16_t bits, int button);
uint16_t set_encoder_pressed(uint16_t bits);
uint16_t set_encoder_released(uint16_t bits);
uint16_t set_encoder_cw(uint16_t bits);
uint16_t set_encoder_ccw(uint16_t bits);

int is_button_pressed(uint16_t bits, int button);
int is_button_released(uint16_t bits, int button);
int is_encoder_pressed(uint16_t bits);
int is_encoder_released(uint16_t bits);
int is_encoder_cw(uint16_t bits);
int is_encoder_ccw(uint16_t bits);

#endif // UI_H

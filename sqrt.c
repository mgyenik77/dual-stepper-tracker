#include "sqrt.h"

float f32_sqrt(float x) {
  // vsqrt does not modify condition codes, only reads and writes registers
  float xr;
  asm ("vsqrt.f32 %0, %1":"=t"(xr):"t"(x));
  return xr;
}

# STM32F4 LCD test code
Derived from https://github.com/martnak/STM32-ILI9341.git

Requires HAL to build. In the project directory, clone STMCubeF4:
$ git clone https://github.com/STMicroelectronics/STM32CubeF4.git

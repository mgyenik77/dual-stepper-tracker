#ifndef COMMAND_H
#define COMMAND_H

#include "buffer.h"
#include "motion.h"

// Simple motion command parser. The host will send motion commands in the form:
// "M <steps> <velocity (steps/s)> <acceleration (steps/second^2)>\n"
//
// This command parser tracks indexes into a buffer and looks for an M and a
// newline, parsing out a full motion command once it detects a newline in the
// buffer and then consumes the bytes used for the command.
struct command_parser {
  int m_found;
  int read_idx;
  int bytes_read;
};

void command_init(struct command_parser* p);

int command_parse(struct command_parser* p, struct buffer* b, struct motion* m);

#endif // COMMAND_H

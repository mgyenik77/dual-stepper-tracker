/* #include <stm32f4xx_hal.h> */

/* #include "imu.h" */

/* struct imu_data current_data; */
/* TIM_HandleTypeDef htim10; */

/* void imu_init() { */
/*   // Initialize GPIOs used to read buttons and encoder */
/*   __HAL_RCC_GPIOD_CLK_ENABLE(); */

/*   GPIO_InitTypeDef GPIO_InitStruct; */
/*   GPIO_InitStruct.Pin = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | */
/*                         GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6; */
/*   GPIO_InitStruct.Mode = GPIO_MODE_INPUT; */
/*   GPIO_InitStruct.Pull = GPIO_PULLUP; */
/*   GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH; */
/*   HAL_GPIO_Init(GPIOD, &GPIO_InitStruct); */

/*   // Initialize timer to poll buttons/encoder */
/*   __HAL_RCC_TIM10_CLK_ENABLE(); */
/*   TIM_ClockConfigTypeDef sClockSourceConfig; */
/*   TIM_MasterConfigTypeDef sMasterConfig; */

/*   htim10.Instance = TIM10; */
/*   htim10.Init.Prescaler = 90-1; */
/*   htim10.Init.CounterMode = TIM_COUNTERMODE_UP; */
/*   htim10.Init.Period = 1000-1; */
/*   htim10.Init.ClockDivision = TIM_CLOCKDIVISION_DIV2; */
/*   htim10.Init.RepetitionCounter = 0; */
/*   if (HAL_TIM_Base_Init(&htim10) != HAL_OK) { */
/*     Error_Handler(); */
/*   } */

/*   sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL; */
/*   if (HAL_TIM_ConfigClockSource(&htim10, &sClockSourceConfig) != HAL_OK) { */
/*     Error_Handler(); */
/*   } */

/*   sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET; */
/*   sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE; */
/*   if (HAL_TIMEx_MasterConfigSynchronization(&htim10, &sMasterConfig) != HAL_OK) { */
/*     Error_Handler(); */
/*   } */

/*   // Start timer and enable interrupts */
/*   HAL_NVIC_SetPriority(TIM10_IRQn, 4, 1); */
/*   HAL_NVIC_EnableIRQ(TIM10_IRQn); */
/*   if (HAL_TIM_Base_Start_IT(&htim10) != HAL_OK) { */
/*     // XXX */
/*   } */
/* } */


/* void TIM10_IRQHandler() { */
/*   if (__HAL_TIM_GET_FLAG(&htim10, TIM_FLAG_UPDATE) == RESET) { */
/*     return; */
/*   } */
/*   if (__HAL_TIM_GET_IT_SOURCE(&htim10, TIM_IT_UPDATE) == RESET) { */
/*     return; */
/*   } */

/*   // Clear interrupt flag so that the interrupt can be triggered again */
/*   __HAL_TIM_CLEAR_IT(&htim10, TIM_IT_UPDATE); */

/*   // XXX */
/* } */

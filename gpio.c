#include "gpio.h"
#include "hw.h"

void gpio_init_output_pp(gpio_t* g) {
  GPIO_InitTypeDef GPIO_InitStruct;
  GPIO_InitStruct.Pin = g->pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(g->port, &GPIO_InitStruct);
}

void gpio_init_input(gpio_t* g, int pullup) {
  GPIO_InitTypeDef GPIO_InitStruct;
  GPIO_InitStruct.Pin = g->pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  if (pullup != 0) {
    GPIO_InitStruct.Pull = GPIO_PULLUP;
  } else {
    GPIO_InitStruct.Pull = GPIO_NOPULL;
  }
  HAL_GPIO_Init(g->port, &GPIO_InitStruct);
}

void gpio_set(gpio_t* g) {
  g->port->BSRR = g->pin;
}

void gpio_reset(gpio_t* g) {
  g->port->BSRR = g->pin << 16;
}

void gpio_assign(gpio_t* g, int high) {
  if (high) {
    gpio_set(g);
  } else {
    gpio_reset(g);
  }
}

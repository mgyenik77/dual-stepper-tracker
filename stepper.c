#include "stepper.h"

DAC_HandleTypeDef DAC_Config;

void init_stepper_common() {
  // Enable peripheral clocks
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_DAC_CLK_ENABLE();

  // Initialize DAC to set chopper current
  DAC_ChannelConfTypeDef Channel_Config;
  DAC_Config.Instance = DAC;
  HAL_DAC_Init(&DAC_Config);

  Channel_Config.DAC_Trigger = DAC_TRIGGER_NONE;
  Channel_Config.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
  HAL_DAC_ConfigChannel(&DAC_Config, &Channel_Config, DAC_CHANNEL_1);
  HAL_DAC_ConfigChannel(&DAC_Config, &Channel_Config, DAC_CHANNEL_2);

  // Configure DAC pins, connect them to DAC
  GPIO_InitTypeDef GPIO_InitStruct;
  GPIO_InitStruct.Pin = GPIO_PIN_4 | GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
}

void stepper_init(stepper_t* s) {
  // Configure outptuts
  gpio_init_output_pp(&s->step);
  gpio_init_output_pp(&s->dir);
  gpio_init_output_pp(&s->decay);
  gpio_init_output_pp(&s->n_reset);
  gpio_init_output_pp(&s->n_sleep);
  gpio_init_output_pp(&s->n_enable);
  gpio_init_output_pp(&s->mode[0]);
  gpio_init_output_pp(&s->mode[1]);
  gpio_init_output_pp(&s->mode[2]);

  // Configure inputs, open collector with pullup
  gpio_init_input(&s->n_fault, PULLUP);
  gpio_init_input(&s->n_home, PULLUP);

  // Enable is active low - we disable the driver while initializing it
  gpio_set(&s->n_enable);

  // XXX
  gpio_set(&s->dir);
  gpio_reset(&s->decay);
  gpio_set(&s->n_reset);
  gpio_set(&s->n_sleep);

  stepper_set_microsteps(s, 0x7);

  // 4096*1.0V/3.3V ~= 1240, output about 1V for 1A of chopper current
  const uint16_t dac_value = 1240;
  const uint32_t dac_channel = s->dac_channel;
  HAL_DAC_SetValue(&DAC_Config, dac_channel, DAC_ALIGN_12B_R, dac_value);
  HAL_DAC_Start(&DAC_Config, dac_channel);

  // Now enable the motor
  gpio_reset(&s->n_enable);
}

void stepper_set_microsteps(struct stepper* s, int microsteps) {
  gpio_assign(&s->mode[0], (microsteps & 0x01));
  gpio_assign(&s->mode[1], (microsteps & 0x02));
  gpio_assign(&s->mode[2], (microsteps & 0x04));
}

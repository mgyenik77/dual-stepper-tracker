#ifndef MOTION_CONTROLLER_H
#define MOTION_CONTROLLER_H

#include "motion.h"
#include "motion_segment.h"
#include "stepper.h"

// This tracks motion for azimuth motor "AZ" and elevation motor "EL"
#define AZ 0
#define EL 1

struct motor_state {
  uint8_t idle;
  uint8_t last;
  struct segment_queue sq;
  struct stepper* stepper;
};

struct motion_controller {
  struct motor_state motors[2];
};

void motion_init(stepper_t* m1, stepper_t* m2);

int motion_do(int motor, struct motion* m);

void motion_stop_all();

#endif // MOTION_CONTROLLER_H

#ifndef _STEPPER_H
#define _STEPPER_H

#include "gpio.h"

#include <stm32f4xx_hal.h>

typedef struct stepper {
  uint32_t dac_channel;
  gpio_t step;
  gpio_t dir;
  gpio_t decay;
  gpio_t n_fault;
  gpio_t n_sleep;
  gpio_t n_reset;
  gpio_t n_enable;
  gpio_t n_home;
  gpio_t mode[3];
} stepper_t;

void init_stepper_common();

void stepper_init(stepper_t* s);

void stepper_set_microsteps(struct stepper* s, int microsteps);

#endif  // _STEPPER_H

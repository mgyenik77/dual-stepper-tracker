#include "nmea.h"

void nmea_init(struct nmea* p) {
  p->idx = 0;
  p->fix = 0;
  p->lat = 0;
  p->lon = 0;
  p->in_sentence = 0;
}

int nmea_parse(struct nmea* p, struct buffer* b) {
  int size = buffer_available(b);

  int chars_consumed = 0;
  int updated = 0;

  if (!p->in_sentence) {
    for (int i = 0; i < size; ++i) {
      chars_consumed++;
      char c = buffer_peek(b, i);
      if (c == '$') {
        p->in_sentence = 1;
        p->idx = 0;
        break;
      }
    }
  }

  // We could also reach here after breaking out of the loop above. That's why
  // we use the same i variable, so that if we already looked through some
  // chars, we don't look through them again.
  if (p->in_sentence) {
    for (int i = chars_consumed; i < size; ++i) {
      chars_consumed++;
      char c = buffer_peek(b, i);
      if (c == 0x0d) {
        p->buffer[p->idx++] = '\0';
        p->in_sentence = 0;
        updated = 1;
        break;
      }
      p->buffer[p->idx++] = c;
    }
  }

  buffer_consume(b, chars_consumed);
  return updated;
}

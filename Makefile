CC      = arm-none-eabi-gcc
LD      = arm-none-eabi-gcc
AR      = arm-none-eabi-ar
AS      = arm-none-eabi-as
OBJCOPY = arm-none-eabi-objcopy
OBJDUMP = arm-none-eabi-objdump
SIZE    = arm-none-eabi-size

SRCS  = $(wildcard ./*.c)
SRCS += $(wildcard ILI9341/*.c)

OBJS  = $(SRCS:.c=.o)
OBJS += startup_stm32f427xx.o

## Platform and optimization options
#CFLAGS += -c -fno-common -Os -g -mcpu=cortex-m4 -mthumb
#CFLAGS += -Wall -ffunction-sections -fdata-sections -fno-builtin
#CFLAGS += -Wno-unused-function -ffreestanding
AFLAGS =  -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16

CFLAGS += -c -O3 -g
CFLAGS += -Wall -ffunction-sections -fdata-sections
CFLAGS += -Wno-unused-function
CFLAGS += -DSTM32F427xx
CFLAGS += -g3
CFLAGS += $(AFLAGS)

LFLAGS  = $(AFLAGS)
LFLAGS  += -Tstm32f427vgt6.ld -nostartfiles -Wl,--gc-sections
#LFLAGS += -specs=nano.specs -specs=nosys.specs

## HAL and CMSIS libraries
CMSIS     = /home/m/git/STM32CubeF4/Drivers/CMSIS
HAL       = /home/m/git/STM32CubeF4/Drivers/STM32F4xx_HAL_Driver
HAL_SRC   = $(HAL)/Src
CMSIS_SRC = $(CMSIS)/Device/ST/STM32F4xx/Source/Templates

## Includes for our code and HAL/CMSIS
INC     = -IILI9341/
INC    += -I./
INC    += -I$(CMSIS)/Include/ -I$(HAL)/Inc/
INC    += -I$(CMSIS)/Device/ST/STM32F4xx/Include/
CFLAGS += $(INC)

# drivers/lib sources -> objects -> local object files 
VPATH                = $(HAL_SRC):$(CMSIS_SRC)

CORE_OBJ_SRC         = $(wildcard $(HAL_SRC)/*.c)
CORE_OBJ_SRC        += $(wildcard $(CMSIS_SRC)/*.c)

CORE_LIB_OBJS        = $(CORE_OBJ_SRC:.c=.o)
CORE_LOCAL_LIB_OBJS  = $(notdir $(CORE_LIB_OBJS))

OPENOCDDIR = /usr/share/openocd/scripts

## Rules
all: size main.bin

core.a: $(CORE_OBJ_SRC)
	$(MAKE) $(CORE_LOCAL_LIB_OBJS)
	$(AR) rcs core.a $(CORE_LOCAL_LIB_OBJS)
	rm -f $(CORE_LOCAL_LIB_OBJS)

main.elf: $(OBJS) core.a stm32f427vgt6.ld
	$(LD) $(LFLAGS) -o main.elf $(OBJS) core.a 

%.bin: %.elf
	$(OBJCOPY) --strip-unneeded -O binary $< $@

flash: main.bin
	st-flash --reset write $< 0x8000000 

ocdflash: main.bin
	openocd \
	-s $(OPENOCDDIR) \
	-f $(OPENOCDDIR)/interface/stlink-v2-1.cfg \
	-f $(OPENOCDDIR)/target/stm32f4x.cfg \
	-c "init" -c "halt" -c "sleep 200" \
	-c "flash erase_address 0x08000000 32768" \
	-c "flash write_image erase $< 0x08000000" \
	-c "verify_image $< 0x08000000" \
	-c "sleep 200" -c "reset run" -c "shutdown"



size: main.elf
	$(SIZE) $< 

clean:
	-rm -f $(OBJS) main.lst main.elf main.hex main.map main.bin main.list

distclean: clean
	-rm -f *.o core.a $(CORE_LIB_OBJS) $(CORE_LOCAL_LIB_OBJS) 

.PHONY: all flash size clean distclean

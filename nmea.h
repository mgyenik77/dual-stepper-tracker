#ifndef NMEA_H
#define NMEA_H

#include "buffer.h"

// Simple NMEA parser - a state machine that parses incoming messages and
// remembers if GPS receiver has a fix, and what the latest lat/lon are
struct nmea {
  // For simplicity, buffer partial messages here until we've received a
  // complete sentence. Sentences have a max length of 79 bytes, but 80 is a
  // nice round number, and it can be null terminated.
  char buffer[80];
  int idx;

  // Keeps track of whether we are waiting to see a '$' or accumulating chars
  // looking for 0x0d.
  int in_sentence;

  // Remember latest lat/lon values and fix info. If we don't have a fix,
  // lat/lon contain undefined values.
  int fix;
  float lat;
  float lon;
};

// Initializes NMEA parser (does not clear buffer contents, just position).
void nmea_init(struct nmea* p);

// Buffers size bytes of data from s, and if a new sentence is detected, parses
// it. Returns 0 if no new sentence was parsed, and 1 if a new sentence was
// parsed and the fix/lat/lon are updated.
int nmea_parse(struct nmea* p, struct buffer* b);

#endif // NMEA_H

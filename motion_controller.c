#include <stm32f4xx_hal.h>

#include "gpio.h"
#include "sqrt.h"
#include "motion_controller.h"

struct motion_controller planner;

// NOTE: Does not support:
//  - v_final different from v_init
//  - different accel and decel rates
//
// These would be easy to add but shouldn't be needed for this application
//
// TODO: handle overflow of segment queue
void plan_point2point_move(struct segment_queue* sq, struct motion* m) {
  // Acceleration and deceleration are two triangles on a graph of velocity vs
  // time, and their area is the number of steps to fully accelerate up to v_max
  // and back down. If this number of steps is more than the number of steps we
  // need to take, we don't even have time to fully accelerate, and we do a
  // triagular move. Otherwise we do a trapezoidal move, and coast in between
  // accel and decel.
  float v_diff = m->v_max - m->v_init;
  float time_to_max_v = v_diff/m->accel;
  float accel_decel_steps = v_diff*time_to_max_v;

  // If we don't have time, do a triangular move, two segments.
  if (accel_decel_steps >= m->steps) {
    int steps_for_accel = m->steps/2;
    int steps_for_decel = m->steps - steps_for_accel;
    segment_queue_add(sq, ACCEL, steps_for_accel, m);
    segment_queue_add(sq, DECEL, steps_for_decel, m);
    return;
  }

  // Otherwise we do a coasting move, consisting of 3 segments.
  int steps_for_accel = accel_decel_steps/2;
  int steps_for_decel = steps_for_accel;
  int steps_for_coast = m->steps - steps_for_accel - steps_for_decel;
  segment_queue_add(sq, ACCEL, steps_for_accel, m);
  segment_queue_add(sq, COAST, steps_for_coast, m);
  segment_queue_add(sq, DECEL, steps_for_decel, m);
}

// Returns 1 if we are done and we can shut down the timer.
int finish_step(int motor) {
  struct motor_state* ms = &planner.motors[motor];
  struct segment_queue* sq = &ms->sq;
  struct motion_segment* seg = segment_queue_current(sq);
  seg->steps_taken++;

  if ((seg->m.wait != NULL) && (*seg->m.wait == 0)) {
    // If we were stopped from fully accelerating, we need to decelerate the
    // same number of steps and skip coasting.
    if (seg->type == ACCEL) {
      int steps_accelerated = seg->steps_taken;
      segment_queue_discard_last(sq);
      segment_queue_discard_last(sq);
      seg = segment_queue_current(sq);
      seg->total_steps = steps_accelerated;
    }

    // Stopping a coast segment is easier, we just move on to deceleration.
    if (seg->type == COAST) {
      segment_queue_discard_last(sq);
      seg = segment_queue_current(sq);
    }
  }

  if (seg->steps_taken == seg->total_steps) {
    segment_queue_discard_last(sq);
  }
  int segments_remaining = segment_queue_size(sq);
  if (segments_remaining == 0) {
    return 1;
  }

  return 0;
}

uint32_t get_next_step_period(int motor) {
  struct motor_state* ms = &planner.motors[motor];
  struct segment_queue* sq = &ms->sq;

  /* int segments_remaining = segment_queue_size(sq); */
  /* if (segments_remaining == 0) { */
  /*   // If we have no more segments, we have completed the motion commands and */
  /*   // are now idle. */
  /*   return 0; */
  /* } */

  struct motion_segment* seg = segment_queue_current(sq);
  int step = seg->steps_taken;

  // If we are just starting a new motion, change the direction to match the
  // requested direction.
  if ((step == 0) && (seg->type == ACCEL)) {
    if (seg->m.direction == DIRECTION_CCW) {
      gpio_set(&ms->stepper->dir);
    } else {
      gpio_reset(&ms->stepper->dir);
    }
  }

  if (seg->type == COAST) {
    return seg->t0_us;
  }
  float step_scale;
  if (seg->type == ACCEL) {
    step_scale = f32_sqrt(step + 1) - f32_sqrt(step);
  }
  if (seg->type == DECEL) {
    step_scale = f32_sqrt(seg->total_steps - step) - f32_sqrt(seg->total_steps - 1 - step);
  }
  return seg->t0_us*step_scale;
}

void motor_state_init(struct motor_state* ms, struct stepper* s) {
  ms->idle = 1;
  ms->last = 0;
  ms->stepper = s;
  segment_queue_init(&ms->sq);
}

void motion_init(stepper_t* m1, stepper_t* m2) {
  motor_state_init(&planner.motors[0], m1);
  motor_state_init(&planner.motors[1], m2);

  __HAL_RCC_TIM12_CLK_ENABLE();
  TIM12->CR1 = 0;

  // Set prescaler to generate 1us counts, and manually trigger an update to
  // load the prescaler (it is buffered and only changed on timer udpate
  // events).
  TIM12->PSC = 89;
  TIM12->EGR = TIM_EGR_UG;
  TIM12->SR = 0;

  // Set the ARR to the time between pulses. This will not be buffered if we do
  // not set the ARPE bit in TIM12
  TIM12->ARR = 99;

  // Set the CCR register to the width of the pulse to generate (DRV8825
  // datasheet wants at least 1.9us high, we do 10us because we have plenty of
  // time)
  TIM12->CCR2 = 10;

  // Enable update event interrupt generation
  TIM12->DIER = TIM_DIER_UIE | TIM_DIER_CC2IE;
  NVIC_SetPriority(TIM8_BRK_TIM12_IRQn, 0);
  NVIC_EnableIRQ(TIM8_BRK_TIM12_IRQn);

}

void motion_stop_all(struct motion_controller* mc) {
  // Emergency stop, disable timers and clear motion queues
  TIM12->CR1 &= ~TIM_CR1_CEN;

  motion_init(planner.motors[0].stepper, planner.motors[1].stepper);
}

int motion_do(int motor, struct motion* m) {
  struct motor_state* ms = &planner.motors[motor];
  struct segment_queue* sq = &ms->sq;

  plan_point2point_move(sq, m);

  // If this is the first motion and we're idle, we need to get things running
  // again. We start the timer, and generate an update event to compute and fill
  // in the ARR. We start the ARR with a large value, plenty of time for the
  // interrupt handler to run and update it.
  if (planner.motors[motor].idle) {
    planner.motors[motor].idle = 0;
    planner.motors[motor].last = 0;
    TIM12->ARR = 10000;
    TIM12->CR1 = TIM_CR1_CEN;
    TIM12->EGR = TIM_EGR_UG;
  }
}

void TIM8_BRK_TIM12_IRQHandler(void) {
  /* TIM12->SR = 0; */
  /* GPIOB->ODR ^= 0x8000; */

  struct motor_state* ms = &planner.motors[AZ];

  if ((TIM12->SR & TIM_SR_CC2IF) != 0) {
    gpio_reset(&ms->stepper->step);
    TIM12->SR &= ~TIM_SR_CC2IF;
    TIM12->CCR2--;

    // The step period calculation routine returns 0 both during and after the
    // last step. If it's during the last step, we are not yet idle, and we want
    // to set the OPM bit to automatically stop the timer once this step is
    // complete, otherwise the timer will reload and go high again, generating an
    // extra step and overshooting the motion.
    int done = finish_step(AZ);
    if (done) {
      TIM12->CR1 = 0;
      ms->idle = 1;
    }
    return;
  }

  // TODO: Check that the interrupt was actually generated by an update event

  gpio_set(&ms->stepper->step);

  // Clear interrupt flag
  TIM12->SR &= ~TIM_SR_UIF;

  uint32_t count = get_next_step_period(AZ);
  if (count > 60000) {
    count = 60000;
  }

  TIM12->ARR = count - 1;
  TIM12->CCR2 = count/2;
}

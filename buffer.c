#include "buffer.h"

void buffer_init(struct buffer* b) {
  b->read = 0;
  b->write = 0;
}

void buffer_write(struct buffer* b, char c) {
  b->mem[b->write++] = c;
  b->write &= 0x1ff;
}

int buffer_available(struct buffer* b) {
  int size = b->write - b->read;
  if (size < 0) {
    size += 512;
  }
  return size;
}

char buffer_peek(struct buffer* b, int n) {
  int idx = b->read + n;
  idx &= 0x1ff;
  return b->mem[idx];
}

void buffer_consume(struct buffer* b, int n) {
  int idx = b->read + n;
  idx &= 0x1ff;
  b->read = idx;
}

#ifndef _GPIO_H
#define _GPIO_H

#include <stm32f4xx_hal.h>

typedef struct gpio {
  GPIO_TypeDef* port;
  uint16_t pin;
} gpio_t;

// Configure GPIO as push-pull output
void gpio_init_output_pp(gpio_t* g);

// Configure GPIO as high-impedance input, with or without internal pullup
// resistor enabled
#define NO_PULLUP 0
#define PULLUP 1
void gpio_init_input(gpio_t* g, int pullup);

// Set GPIO high
void gpio_set(gpio_t* g);

// Set GPIO low
void gpio_reset(gpio_t* g);

// Set gpio to 1 or 0
void gpio_assign(gpio_t* g, int high);

#endif  // _GPIO_H

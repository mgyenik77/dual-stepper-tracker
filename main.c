#include "gpio.h"
#include "tim.h"
#include "usart.h"
#include "menu.h"
#include "nmea.h"
#include "ui.h"
#include "motion.h"
#include "motion_controller.h"
#include "gps.h"

#include <stm32f4xx_hal.h>

#include <ILI9341_GFX.h>
#include <ILI9341_STM32_Driver.h>
#include <ILI9341_Touchscreen.h>

#include "gpio.h"
#include "stepper.h"
#include "hw.h"
#include <stdio.h>

#define sprintf(...)

void SystemClock_Config(void);
void Error_Handler(void);

void _init() {}

void *memcpy(void *dest, const void *src, size_t n) {
  size_t i;

  for (i = 0; i < n; ++i)
    ((unsigned char *)dest)[i] = ((unsigned char *)src)[i];

  return dest;
}

void *memset(void *blk, int c, size_t n) {
  size_t i;

  for (i = 0; i < n; ++i)
    ((unsigned char *)blk)[i] = c;

  return blk;
}

stepper_t m1 = {
  .dac_channel = DAC_CHANNEL_1,
  .step = {GPIOC, GPIO_PIN_8},
  .dir = {GPIOA, GPIO_PIN_8},
  .decay = {GPIOA, GPIO_PIN_9},
  .n_fault = {GPIOA, GPIO_PIN_10},
  .n_sleep = {GPIOA, GPIO_PIN_11},
  .n_reset = {GPIOA, GPIO_PIN_12},
  .n_enable = {GPIOC, GPIO_PIN_9},
  .n_home = {GPIOD, GPIO_PIN_14},
  .mode = {
    {GPIOC, GPIO_PIN_7},
    {GPIOC, GPIO_PIN_6},
    {GPIOD, GPIO_PIN_15},
  },
};

stepper_t m2 = {
  .dac_channel = DAC_CHANNEL_2,
  .step = {GPIOB, GPIO_PIN_15},
  .dir = {GPIOD, GPIO_PIN_9},
  .decay = {GPIOD, GPIO_PIN_10},
  .n_fault = {GPIOD, GPIO_PIN_11},
  .n_sleep = {GPIOD, GPIO_PIN_12},
  .n_reset = {GPIOD, GPIO_PIN_13},
  .n_enable = {GPIOD, GPIO_PIN_8},
  .n_home = {GPIOB, GPIO_PIN_11},
  .mode = {
    {GPIOB, GPIO_PIN_14},
    {GPIOB, GPIO_PIN_13},
    {GPIOB, GPIO_PIN_12},
  },
};

int main(void) {
  struct nmea nmea_parser;
  nmea_init(&nmea_parser);

  HAL_Init();

  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);

  SystemClock_Config();

  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();

  ui_init();
  gps_init();

  init_stepper_common();
  stepper_init(&m1);
  stepper_init(&m2);

  motion_init(&m1, &m2);

  HAL_GPIO_WritePin(GPIOE, LCD_CS_PIN, GPIO_PIN_SET);
  HAL_Delay(20);
  ILI9341_Init();
  HAL_GPIO_WritePin(GPIOE, LCD_CS_PIN, GPIO_PIN_SET);
  HAL_Delay(20);

  ILI9341_Set_Rotation(SCREEN_HORIZONTAL_1);

  ILI9341_Fill_Screen(RED);
  ILI9341_Draw_Text("Leah Lackey", 10, 10, BLUE, 2, RED);
  ILI9341_Draw_Text("EE492 Protoype", 10, 50, BLUE, 2, RED);

  int menu = menus_init();

  while (1) {
    uint16_t events = ui_get_events();
    menu = menus[menu].event_handler(events);
    menus[menu].draw();

    // Drain GPS uart buffer into nmea parser and possibly see updated lat/lon
    int updated = nmea_parse(&nmea_parser, gps_rx_buffer());
    if (updated) {
      ILI9341_Fill_Screen(RED);
      ILI9341_Draw_Text(nmea_parser.buffer, 10, 10, BLUE, 1, RED);
    }

  }
}

/**
 * @brief  System Clock Configuration
 *         The system Clock is configured as follow :
 *            System Clock source            = PLL (HSE)
 *            SYSCLK(Hz)                     = 180000000
 *            HCLK(Hz)                       = 180000000
 *            AHB Prescaler                  = 1
 *            APB1 Prescaler                 = 4
 *            APB2 Prescaler                 = 2
 *            HSE Frequency(Hz)              = 8000000
 *            PLL_M                          = 8
 *            PLL_N                          = 360
 *            PLL_P                          = 2
 *            PLL_Q                          = 7
 *            VDD(V)                         = 3.3
 *            Main regulator output voltage  = Scale1 mode
 *            Flash Latency(WS)              = 5
 * @param  None
 * @retval None
 */
void SystemClock_Config(void) {
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};

  /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();

  /* The voltage scaling allows optimizing the power consumption when the device
     is clocked below the maximum system frequency, to update the voltage
     scaling value regarding system frequency refer to product datasheet.  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */

  /* Enable HSE Oscillator and activate PLL with HSE as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 360;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
    /* Initialization Error */
    Error_Handler();
  }

  if (HAL_PWREx_EnableOverDrive() != HAL_OK) {
    /* Initialization Error */
    Error_Handler();
  }

  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK |
                                 RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK) {
    /* Initialization Error */
    Error_Handler();
  }
}

__attribute__((noinline)) void Error_Handler(void) {
  while (1) {
  }
}

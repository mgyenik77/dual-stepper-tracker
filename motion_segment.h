#ifndef MOTION_SEGMENT_H
#define MOTION_SEGMENT_H

#include <stdint.h>

#include "motion.h"

// We break every motion down into segments of either constant acceleration,
// constant velocity (coasting), or constant deceleration. Each of these pieces
// of motion is referred to as a "motion segment" or "segment".
#define ACCEL 0
#define DECEL 1
#define COAST 2
struct motion_segment {
  struct motion m;
  int type;
  int steps_taken;
  int total_steps;
  uint32_t t0_us;
};

// We keep an interrupt safe ring queue of segments, which are executed in FIFO
// order. The segment queue is written to by the main loop, and read from by the
// timer interrupt handlers.
struct segment_queue {
  struct motion_segment segments[MAX_SEGMENTS];
  int read;
  int write;
};

// Initializes a segment queue to be empty
void segment_queue_init(struct segment_queue* sq);

// Adds a segment of the given type and step length to the given queue, ot be
// performed with the given motion parameters.
//
// TODO: Support constant velocity moves with no defined length in steps
void segment_queue_add(struct segment_queue* sq, int type, int steps, struct motion* m);

// Removes the last segment from the segment queue, used by the motion
// controller interrupt handler to discard a segment after it has been performed
void segment_queue_discard_last(struct segment_queue* sq);

// Returns the number of segments still in the queue that have yet to be fully
// executed
int segment_queue_size(struct segment_queue* sq);

// Returns the segment currently being executed (the one least recently added to the queue)
struct motion_segment* segment_queue_current(struct segment_queue* sq);

#endif // MOTION_SEGMENT_H

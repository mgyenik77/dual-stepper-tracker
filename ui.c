#include <stm32f4xx_hal.h>

#include "ui.h"
#include "gpio.h"

volatile uint16_t global_events;

TIM_HandleTypeDef htim7;

// This contains the last 8 readings of the GPIO pin. If there is a solid
// stream of 1s or 0s, the button is not bouncing.
uint8_t debounce[7];

// Maintains a boolean value for whether or not the button was pressed the
// last time it was checked. If the debounce state does not match the pressed
// state here, the button was either pressed or released.
uint8_t pressed[7];

uint16_t set_button_pressed(uint16_t bits, int button) {
  int bit = button;
  return bits | (1 << bit);
}
uint16_t set_button_released(uint16_t bits, int button) {
  int bit = button + 4;
  return bits | (1 << bit);
}
uint16_t set_encoder_pressed(uint16_t bits) {
  int bit = 8;
  return bits | (1 << bit);
}
uint16_t set_encoder_released(uint16_t bits){
  int bit = 9;
  return bits | (1 << bit);
}
uint16_t set_encoder_cw(uint16_t bits) {
  int bit = 10;
  return bits | (1 << bit);
}
uint16_t set_encoder_ccw(uint16_t bits) {
  int bit = 11;
  return bits | (1 << bit);
}

int is_button_pressed(uint16_t bits, int button) {
  int bit = button;
  return (bits & (1 << bit)) != 0;
}
int is_button_released(uint16_t bits, int button) {
  int bit = button + 4;
  return (bits & (1 << bit)) != 0;
}
int is_encoder_pressed(uint16_t bits) {
  int bit = 8;
  return (bits & (1 << bit)) != 0;
}
int is_encoder_released(uint16_t bits){
  int bit = 9;
  return (bits & (1 << bit)) != 0;
}
int is_encoder_cw(uint16_t bits) {
  int bit = 10;
  return (bits & (1 << bit)) != 0;
}
int is_encoder_ccw(uint16_t bits) {
  int bit = 11;
  return (bits & (1 << bit)) != 0;
}

void ui_init() {
  // Initialize GPIOs used to read buttons and encoder
  __HAL_RCC_GPIOD_CLK_ENABLE();

  GPIO_InitTypeDef GPIO_InitStruct;
  GPIO_InitStruct.Pin = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 |
                        GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  // Initialize timer to poll buttons/encoder
  __HAL_RCC_TIM7_CLK_ENABLE();
  TIM_ClockConfigTypeDef sClockSourceConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim7.Instance = TIM7;
  htim7.Init.Prescaler = 90-1;
  htim7.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim7.Init.Period = 1000-1;
  htim7.Init.ClockDivision = TIM_CLOCKDIVISION_DIV2;
  htim7.Init.RepetitionCounter = 0;
  if (HAL_TIM_Base_Init(&htim7) != HAL_OK) {
    Error_Handler();
  }

  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim7, &sClockSourceConfig) != HAL_OK) {
    Error_Handler();
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim7, &sMasterConfig) != HAL_OK) {
    Error_Handler();
  }

  // Start timer and enable interrupts
  HAL_NVIC_SetPriority(TIM7_IRQn, 5, 1);
  HAL_NVIC_EnableIRQ(TIM7_IRQn);
  if (HAL_TIM_Base_Start_IT(&htim7) != HAL_OK) {
    // XXX
  }
}

uint16_t ui_get_events() {
  uint16_t events;
  __disable_irq();
  events = global_events;
  global_events = 0;
  __enable_irq();
  return events;
}

void TIM7_IRQHandler() {
  if (__HAL_TIM_GET_FLAG(&htim7, TIM_FLAG_UPDATE) == RESET) {
    return;
  }
  if (__HAL_TIM_GET_IT_SOURCE(&htim7, TIM_IT_UPDATE) == RESET) {
    return;
  }

  // Clear interrupt flag so that the interrupt can be triggered again
  __HAL_TIM_CLEAR_IT(&htim7, TIM_IT_UPDATE);

  // Read and debounce buttons. The 4 buttons are the pins 0-3 on port D.
  int pins[4] = {GPIO_PIN_0, GPIO_PIN_1, GPIO_PIN_2, GPIO_PIN_3};

  for (int i = 0; i < 4; ++i) {
    // First shift away the last bit and make room for the new bit
    debounce[i] = debounce[i] << 1;

    // If the pin is high, set the new bit to 1. Otherwise leave it 0.
    if (HAL_GPIO_ReadPin(GPIOD, pins[i])) {
      debounce[i] |= 1;
    }

    // If it's all 1's, the button is not being held down. If it was considered
    // pressed the last time we checked, this means it was released.
    if (debounce[i] == 0x7f) {
      global_events = set_button_released(global_events, i);
    }

    // If it's all 0's, the button is being held down. If it was considered not
    // pressed the last time we checked, this means it was pressed.
    if (debounce[i] == 0x80) {
      global_events = set_button_pressed(global_events, i);
    }
  }

}

#ifndef BUFFER_H
#define BUFFER_H

// Simple ISR safe circular buffer for receiving UART bytes and storing them for
// later processing.
struct buffer {
  char mem[512];
  int read;
  int write;
};

// Initializes buffer to be empty (does not clear bytes).
void buffer_init(struct buffer* b);

// Writes one char to the buffer, called from ISR.
void buffer_write(struct buffer* b, char c);

// Returns the current size of the buffer (the number of bytes that have not
// been consumed).
int buffer_available(struct buffer* b);

// Returns the Nth unconsumed byte.
char buffer_peek(struct buffer* b, int n);

// Consumes N bytes.
void buffer_consume(struct buffer* b, int n);

#endif // BUFFER_H

#ifndef IMU_H
#define IMU_H

struct vec {
  int x;
  int y;
  int z;
};

struct imu_data {
  struct vec dmp_posiiton;
};

void imu_init();

struct imu_data get_current_data();

#endif // IMU_H

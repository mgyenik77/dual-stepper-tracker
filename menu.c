#include "menu.h"
#include "motion.h"
#include "motion_controller.h"
#include "ui.h"

#include <ILI9341_GFX.h>
#include <ILI9341_STM32_Driver.h>
#include <ILI9341_Touchscreen.h>

#define MAIN_MENU 0
#define JOG_MENU 1

struct menu menus[2];

struct jog_state {
  uint8_t wait;
  int motor;
};

void jog_state_init(struct jog_state* js) {
  js->wait = 0;
  js->motor = 0;
}

struct jog_state jog_menu_state;

void main_draw(void) {
  ILI9341_Fill_Screen(DARKCYAN);
  //  ILI9341_Draw_Text(button_text[i], 10, 10, BLUE, 2, RED);
}

void jog_draw(void) {
  ILI9341_Fill_Screen(CYAN);
  //  ILI9341_Draw_Text(button_text[i], 10, 10, BLUE, 2, RED);
}

int main_event_handler(uint16_t events) {
  if (is_button_pressed(events, 0)) {
    jog_state_init(&jog_menu_state);
    return JOG_MENU;
  }
  return MAIN_MENU;
}

int jog_event_handler(uint16_t events) {
  if (is_button_pressed(events, 3)) {
    return MAIN_MENU;
  }

  if (is_button_pressed(events, 0)) {
    jog_menu_state.wait = 1;
    struct motion m;
    m.steps = 100000;
    m.direction = DIRECTION_CW;
    m.v_init = 0;
    m.v_max = 5000;
    m.accel = 9000;
    m.wait = &jog_menu_state.wait;
    motion_do(AZ, &m);
  }
  if (is_button_released(events, 0)) {
    jog_menu_state.wait = 0;
  }

  if (is_button_pressed(events, 1)) {
    jog_menu_state.wait = 1;
    struct motion m;
    m.steps = 100000;
    m.direction = DIRECTION_CCW;
    m.v_init = 0;
    m.v_max = 5000;
    m.accel = 9000;
    m.wait = &jog_menu_state.wait;
    motion_do(AZ, &m);
  }
  if (is_button_released(events, 1)) {
    jog_menu_state.wait = 0;
  }

  return JOG_MENU;
}

int menus_init(void) {
  menus[MAIN_MENU] = (struct menu){
    .button_labels = {"one", "two", "three", "four"},
    .event_handler = main_event_handler,
    .draw = main_draw,
  };
  menus[JOG_MENU] = (struct menu){
    .button_labels = {"CW", "CCW", "Motor", "Back"},
    .event_handler = jog_event_handler,
    .draw = jog_draw,
  };

  return MAIN_MENU;
}

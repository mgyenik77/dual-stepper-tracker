#include "command.h"

void command_init(struct command_parser* p) {
  p->m_found = 0;
  p->read_idx = 0;
  p->bytes_read = 0;
}

int command_parse(struct command_parser* p, struct buffer* b, struct motion* m) {
  if (!p->m_found) {
    uint32_t bytes_available = buffer_available(b);
    for (uint32_t i = 0; i < bytes_available; ++i) {
      char c = buffer_peek(b, 0);
      if (c == 'M') {
        p->m_found = 1;
        break;
      }
      buffer_consume(b, 1);
    }

    if (!p->m_found) {
      return;
    }
  }

  if (p->m_found) {
    uint32_t bytes_available = buffer_available(b);
    for (uint32_t i = 0; i < bytes_available; ++i) {
      char c = buffer_peek(b, i);
    }
  }
}

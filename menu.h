#ifndef MENU_H
#define MENU_H

#include <stdint.h>

struct menu {
  // Each menu has a different use for the buttons, so each menu has a set of
  // labels for each one
  char* button_labels[4];

  // Each menu has a function pointer which points to a function that will
  // perform some actions when buttons are pressed, and return the index of the
  // next menu in the global menus list.
  int (*event_handler)(uint16_t events);

  // Each menu has unique information to display, and requires its own draw
  // function pointer, which points to a function that will update the LCD.
  void (*draw)(void);
};

// There is a global list of menus. Calling menus_init() will return the index
// of the first menu to display on power up, and after that each event handler
// call might update that index if the buttons cause a change in menu state.
int menus_init(void);
extern struct menu menus[2];

#endif // MENU_H

#ifndef GPS_H
#define GPS_H

#include <stm32f4xx_hal.h>

#include "buffer.h"

void gps_init();

struct buffer* gps_rx_buffer();

#endif // GPS_H
